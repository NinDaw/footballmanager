package pl.sdacademy.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.entity.Team;
import pl.sdacademy.ui.gamemenu.GameMenuController;
import pl.sdacademy.ui.gamemenu.GameMenuController;
import pl.sdacademy.util.DataGenerator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by D on 10.05.2017.
 */
public class MainMenuController {
    @FXML
    private Button startButton;
    @FXML
    private Button editorButton;
    @FXML
    private Button continueButton;
    @FXML
    private ChoiceBox<Team> teamChoiceBox;
    @FXML
    private Button dataGenerateButton;

    private Stage stage;

    public void initialize(Stage stage) {
        this.stage = stage;

        teamChoiceBox.setConverter(new StringConverter<Team>() {
            @Override
            public String toString(Team team) {
                return team.getName();
            }

            @Override
            public Team fromString(String string) {
                return null;
            }
        });

        startButton.setOnAction(event -> {

            Team selectedTeam = teamChoiceBox.getSelectionModel().getSelectedItem();
            if(selectedTeam == null) {
                return;
            }
            int teamId = selectedTeam.getId();
            DaoManager.MATCH_DAO.deleteAll();

            Path filePath = Paths.get("teamId.txt");
            String id = Integer.toString(teamId);
            List<String> stringList = Arrays.asList(Integer.toString(teamId));
            try {

                Files.write(filePath,
                        stringList,
                        StandardOpenOption.APPEND);

            } catch (IOException e) {
                e.printStackTrace();
            }
            openGameWindow(teamId);
        });

        editorButton.setOnAction(event -> openEditorWindow());

        dataGenerateButton.setOnAction(event -> {
            DataGenerator.generateTeamsAndPlayers();
            fillTeamChoiceBox();
        });

        continueButton.setOnAction(event -> {
            Path filePath = Paths.get("teamId.txt");

            int teamId;
            try {

                List<String> lines = Files.readAllLines(filePath);
                teamId = Integer.parseInt(lines.get(0));
                openGameWindow(teamId);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });

        if (DaoManager.MATCH_DAO.getAll().size() > 0) {
            continueButton.setVisible(true);
            continueButton.setText("Kontynuuj rozgrywke");
        } else {
            continueButton.setVisible(false);
        }
       // continueButton.setOnAction(event -> );




        fillTeamChoiceBox();
    }

    private void fillTeamChoiceBox() {
        ObservableList<Team> teams = FXCollections.observableArrayList(DaoManager.TEAM_DAO.getAll());
        teamChoiceBox.setItems(teams);

        if(teams.size() > 0) {
            teamChoiceBox.getSelectionModel().select(0);
        }
    }

    private void openEditorWindow() {
        Stage editorStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TeamsEdit.fxml"));
        HBox root = null;
        try {
            root = fxmlLoader.load();
            TeamsEditController controller = fxmlLoader.getController();
            controller.initialize(stage);
            editorStage.setTitle("Football Manager");
            editorStage.setScene(new Scene(root, 602, 500));
            editorStage.show();

            editorStage.setOnCloseRequest(event -> fillTeamChoiceBox());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Błąd FXMLLoadera");
        }
    }

    private void openGameWindow(int teamId) {
        Stage editorStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RoundStats.fxml"));
        //DaoManager.PLAYER_DAO.
        VBox root = null;
        try {
            root = fxmlLoader.load();
            GameMenuController controller = fxmlLoader.getController();
            controller.initialize(editorStage, teamId);
            editorStage.setTitle("Football Manager");
            editorStage.setScene(new Scene(root, 602, 500));
            editorStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Błąd FXMLLoadera");
        }
    }

}
