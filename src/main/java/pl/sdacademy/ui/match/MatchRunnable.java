package pl.sdacademy.ui.match;

import javafx.application.Platform;
import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.dao.PlayerDao;
import pl.sdacademy.entity.Match;
import pl.sdacademy.entity.Player;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

/**
 * Created by D on 11.05.2017.
 */
public class MatchRunnable implements Runnable {
    private Match match;
    private int team1PlayersValue;
    private int team2PlayersValue;
    private List<Player> team1Lineup;
    private List<Player> team2LineUp;

    private PlayerDao playerDao;


    public MatchRunnable(Match match, List<Player> team1Lineup, List<Player> team2LineUp) {
        this.match = match;
        this.team1PlayersValue = evaluatePlayers(team1Lineup);
        this.team2PlayersValue = evaluatePlayers(team2LineUp);
    }

    private int evaluatePlayers(List<Player> players) {
        return players.stream().limit(11)
                .mapToInt(p -> p.getShooting() + p.getSpeed() + p.getStamina())
                .sum();
    }

    @Override
    public void run() {
        for (int i = 0; i <= 90; i++) {
            delay();
            onMinutesUpdate(i);

            if(checkIfTeamScoredGoal(team1PlayersValue)) {
                match.setTeam1Goals(match.getTeam1Goals() + 1);
                onTeam1GoalScored(match.getTeam1Goals());
            }
            if(checkIfTeamScoredGoal(team2PlayersValue)) {
                match.setTeam2Goals(match.getTeam2Goals() + 1);
                onTeam2GoalScored(match.getTeam2Goals());
            }
        }
        onEnd();
    }

    private boolean checkIfTeamScoredGoal(int teamPlayersValue) {
        Random random = new Random();
        int totalValue = 10000 + teamPlayersValue;
        boolean a = totalValue * random.nextInt(100) >= 990000;
        return a && random.nextInt(5) == 0;
    }

    void onMinutesUpdate(int minutes) {
    }

    void delay() {
    }

    void onTeam1GoalScored(int team1Goals) {
    }

    void onTeam2GoalScored(int team2Goals) {
    }

    void onEnd() {
    }

}
