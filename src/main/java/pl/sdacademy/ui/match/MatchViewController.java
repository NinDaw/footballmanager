package pl.sdacademy.ui.match;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.entity.Match;
import pl.sdacademy.entity.Player;
import pl.sdacademy.entity.Team;

import java.util.List;

/**
 * Created by D on 11.05.2017.
 */
public class MatchViewController {
    @FXML
    private Label team1NameLabel;
    @FXML
    private Label team2NameLabel;
    @FXML
    private Label team1GoalsLabel;
    @FXML
    private Label team2GoalsLabel;
    @FXML
    private Label clockLabel;

    public void initialize(Stage stage, Match playersMatch) {
        Team team1 = DaoManager.TEAM_DAO.get(playersMatch.getTeam1Id());
        Team team2 = DaoManager.TEAM_DAO.get(playersMatch.getTeam2Id());

        List<Player> team1LineUp = DaoManager.PLAYER_DAO.getByTeamId(team1.getId());
        List<Player> team2LineUp = DaoManager.PLAYER_DAO.getByTeamId(team1.getId());

        team1NameLabel.setText(team1.getName());
        team2NameLabel.setText(team2.getName());

        new Thread(new MatchRunnable(playersMatch, team1LineUp, team2LineUp) {
            @Override
            void delay() {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            void onMinutesUpdate(final int minutes) {
                Platform.runLater(() -> clockLabel.setText(Integer.toString(minutes)));
            }

            @Override
            void onTeam1GoalScored(final int team1Goals) {
                Platform.runLater(() -> team1GoalsLabel.setText(Integer.toString(team1Goals)));
            }

            @Override
            void onTeam2GoalScored(final int team2Goals) {
                Platform.runLater(() -> team2GoalsLabel.setText(Integer.toString(team2Goals)));
            }

            @Override
            void onEnd() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> stage.close());
            }
        }).start();

        new MatchRunnable(playersMatch, team1LineUp, team2LineUp).run();
    }

    public Label getTeam1GoalsLabel() {
        return team1GoalsLabel;
    }

    public Label getTeam2GoalsLabel() {
        return team2GoalsLabel;
    }

    public Label getClockLabel() {
        return clockLabel;
    }
}
