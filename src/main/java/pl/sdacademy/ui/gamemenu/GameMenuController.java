package pl.sdacademy.ui.gamemenu;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.entity.Match;
import pl.sdacademy.entity.Player;
import pl.sdacademy.entity.Team;
import pl.sdacademy.ui.TeamLineUpController;
import pl.sdacademy.ui.match.MatchRunnable;
import pl.sdacademy.ui.match.MatchViewController;
import pl.sdacademy.util.DataGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GameMenuController {
    @FXML
    private TableColumn<TeamRoundStatsTableRow, Integer> ordinalColumn;
    @FXML
    private TableColumn<TeamRoundStatsTableRow, String> nameColumn;
    @FXML
    private TableColumn<TeamRoundStatsTableRow, Integer> pointsColumn;
    @FXML
    private TableColumn<TeamRoundStatsTableRow, Integer> goalsScoredColumn;
    @FXML
    private TableColumn<TeamRoundStatsTableRow, Integer> goalsConcededColumn;
    @FXML
    private TableView<TeamRoundStatsTableRow> tableView;
    @FXML
    private Label titleLabel;
    @FXML
    private Slider roundNoSlider;
    @FXML
    private Button playNextRoundButton;
    @FXML
    private Button roundTeamEditor;


    private int playerTeamId;

    private Stage stage;

    public void initialize(Stage stage, int playerTeamId) {
        this.stage = stage;
        this.playerTeamId = playerTeamId;
        // Jeśli chcemy dopisać "customową" wartość wyświetlaną w kolumnie, możemy użyć sposobu:
        ordinalColumn.setCellValueFactory(column -> {
            TeamRoundStatsTableRow row = column.getValue();
            return new ReadOnlyObjectWrapper<>(tableView.getItems().indexOf(row) + 1);
        });

        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("points"));
        goalsScoredColumn.setCellValueFactory(new PropertyValueFactory<>("goalsScored"));
        goalsConcededColumn.setCellValueFactory(new PropertyValueFactory<>("goalsConceded"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("teamName"));

        tableView.setRowFactory(tv -> new TableRow<TeamRoundStatsTableRow>() {
            @Override
            public void updateItem(TeamRoundStatsTableRow item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setStyle("");
                } else if (item.getTeamId() == playerTeamId) {
                    setStyle("-fx-background-color: #aaaaaa;");
                } else {
                    setStyle("");
                }
            }
        });

        roundNoSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            showDataForRoundNo(newValue.intValue());
        });
        int lastRoundNo = DaoManager.MATCH_DAO.getLastRoundNo();
        roundNoSlider.setMax(lastRoundNo);
        roundNoSlider.setValue(lastRoundNo);
        showDataForRoundNo(lastRoundNo);
        playNextRoundButton.setOnAction(event -> {
            startNextRound();
        });
       roundTeamEditor.setOnAction(event -> {
           openRoundTeamEditorWindow();
       });
    }

    private void startNextRound() {
        int nextRoundNo = DaoManager.MATCH_DAO.getLastRoundNo() + 1;
        List<Match> roundMatches = DataGenerator.getRoundMatches(nextRoundNo);
        if (!roundMatches.isEmpty()) {
            Optional<Match> optionalPlayersMatch = roundMatches.stream()
                    .filter(m -> m.getTeam1Id() == playerTeamId || m.getTeam2Id() == playerTeamId)
                    .findFirst();
            roundMatches.stream()
                    .filter(m -> !m.equals(optionalPlayersMatch.orElse(null)))
                    .forEach(m -> {
                        List<Player> team1Players = DaoManager.PLAYER_DAO.getByTeamId(m.getTeam1Id());
                        List<Player> team2Players = DaoManager.PLAYER_DAO.getByTeamId(m.getTeam2Id());
                        new MatchRunnable(m, team1Players, team2Players).run();
                        DaoManager.MATCH_DAO.add(m);
                    });

            optionalPlayersMatch.ifPresent(m -> playPlayersMatch(m));
            roundNoSlider.setMax(nextRoundNo);
            roundNoSlider.setValue(nextRoundNo);
            showDataForRoundNo(nextRoundNo);
        }
    }

    private void playPlayersMatch(Match playersMatch) {
        Stage matchStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../MatchView.fxml"));
        HBox root = null;
        try {
            root = fxmlLoader.load();
            MatchViewController controller = fxmlLoader.getController();
            controller.initialize(matchStage, playersMatch);
            matchStage.initModality(Modality.WINDOW_MODAL);
            matchStage.initOwner(stage);
            matchStage.setTitle("Football Manager");
            matchStage.setScene(new Scene(root, 602, 500));
            matchStage.showAndWait();
            DaoManager.MATCH_DAO.add(playersMatch);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDataForRoundNo(int roundNo) {
        titleLabel.setText("Tabela dla kolejki nr " + roundNo);

        List<Team> teams = DaoManager.TEAM_DAO.getAll();
        List<TeamRoundStatsTableRow> tableRows = new ArrayList<>();
        for (Team team : teams) {
            List<Match> teamMatches = DaoManager.MATCH_DAO.getMatchesForTeamTillRound(team.getId(), roundNo);
            tableRows.add(new TeamRoundStatsTableRow(team, teamMatches));
        }
        // sortujemy wiersze, najpierw wedle roznicy punktow, nastepnie wedlug roznicy goli (strzelonych i straconych)
        tableRows = tableRows.stream()
                .sorted((tr1, tr2) -> {
                    int pointsDifference = tr2.getPoints() - tr1.getPoints();
                    if (pointsDifference != 0) {
                        return pointsDifference;
                    } else {
                        return (tr2.getGoalsScored() - tr2.getGoalsConceded())
                                - (tr1.getGoalsScored() - tr1.getGoalsConceded());
                    }
                })
                .collect(Collectors.toList());


        tableView.setItems(FXCollections.observableArrayList(tableRows));
    }

    private void openRoundTeamEditorWindow() {
        Stage editorStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../TeamLineUp.fxml"));
        VBox root = null;
        try {
            root = fxmlLoader.load();
            TeamLineUpController controller = fxmlLoader.getController();
            controller.initialize(stage, playerTeamId);
            editorStage.setTitle("Football Manager");
            editorStage.setScene(new Scene(root, 602, 500));
            editorStage.show();


        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Błąd FXMLLoadera");
        }
    }
}
