package pl.sdacademy.ui;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.entity.Player;
import pl.sdacademy.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-16.
 */
public class TeamLineUpController {
    @FXML
    Label titleLabel;
    @FXML
    ListView<Player> teamListView;
    @FXML
    Button saveButton;
    private Stage stage;
    private int playerTeamId;
    private List<Player> players;

    public void initialize(Stage stage, int playerTeamId) {
        this.stage = stage;
        this.playerTeamId = playerTeamId;
        this.players = DaoManager.PLAYER_DAO.getByTeamId(playerTeamId);
        players.addAll(players);

        teamListView.setItems(FXCollections.observableArrayList(players));

        teamListView.setCellFactory(param -> new ListCell<Player>() {
            @Override
            protected void updateItem(Player player, boolean empty) {
                super.updateItem(player, empty);
                if (empty || player == null) {
                    setText(null);
                } else {
                    String playerCellText = Utils.ellipsize(player.getName() + " " + player.getSurname(), 30);
                    int id = players.indexOf(player);
                    setText((id+1)+" "+playerCellText);
                }
            }
        });

    }

    public List<Player> getLineUp(){
        List<Player> list = new ArrayList<>();
return list;
    }
}
//5. Utwórz widok TeamLineUp, który będzie miał kontrolkę ListView, w której wyświetleni będą piłkarze drużyny.
// Kolejność piłkarzy powinna móc ulegać zmianie, piłkarz powinien być wyświetlany na liście w sposób następujący:
// <LP>
// <imie>
// <nazwisko>.
// Kontroler do widoku powinien przyjąć za parametr id drużyny, oraz posiadać metodę getLineUp,
// która zwróci listę piłkarzy w odpowiedniej kolejności.