package pl.sdacademy.dao;

import pl.sdacademy.entity.Team;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by i318523 on 5/16/17.
 */
public class TeamIDDao {
    private final static String FILE_NAME = "teamId.txt";
    private Team teamId;



    public TeamIDDao(Team teamId) {
        this.teamId = teamId;

        Path filePath = Paths.get("teamId.txt");

        String id = teamId.getId().toString();

        List<String> stringList = Arrays.asList(teamId.getId().toString());

        try {

            Files.write(filePath,
                    stringList,
                    StandardOpenOption.APPEND);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static String getFileName() {
        return FILE_NAME;
    }

    public Team getTeamId() {
        return teamId;
    }

    public void setTeamId(Team teamId) {
        this.teamId = teamId;
    }

}
