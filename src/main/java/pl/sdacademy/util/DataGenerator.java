package pl.sdacademy.util;

import pl.sdacademy.dao.DaoManager;
import pl.sdacademy.dao.MatchDao;
import pl.sdacademy.entity.Match;
import pl.sdacademy.entity.Player;
import pl.sdacademy.entity.Team;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by D on 08.05.2017.
 */
public class DataGenerator {

    public static List<Match> getRoundMatches(int roundNo) {
        List<Match> matches = DaoManager.MATCH_DAO.getAll();
        List<Integer> teamIds = DaoManager.TEAM_DAO
                .getAll()
                .stream()
                .map(t -> t.getId())
                .collect(Collectors.toList());

        List<Match> thisRoundMatches = new ArrayList<>();

        List<Integer> teamIdsThatDidNotPlayThisRound = new ArrayList<>(teamIds);

        boolean cannotPlayAnotherMatchThisRound = false;
        while (teamIdsThatDidNotPlayThisRound.size() > 0 && !cannotPlayAnotherMatchThisRound) {
            boolean matchWasPlayed = false;
            for(int j = 0; j < teamIdsThatDidNotPlayThisRound.size(); j++) {
                int team1Id = teamIdsThatDidNotPlayThisRound.get(j);
                for(int k = j + 1; k < teamIdsThatDidNotPlayThisRound.size(); k++) {
                    int team2Id = teamIdsThatDidNotPlayThisRound.get(k);
                    if(!checkIfTeamsPlayedMatch(matches, team1Id, team2Id)) {
                        teamIdsThatDidNotPlayThisRound.remove(new Integer(team1Id));
                        teamIdsThatDidNotPlayThisRound.remove(new Integer(team2Id));

                        Match match = new Match(team1Id, team2Id, 0, 0, roundNo);
                        thisRoundMatches.add(match);
                        matches.add(match);
                        matchWasPlayed = true;
                        break;
                    }
                }
                if(matchWasPlayed) {
                    break;
                }
            }
            if(!matchWasPlayed) {
                cannotPlayAnotherMatchThisRound = true;
            }
        }
        return thisRoundMatches;
    }

    private static boolean checkIfTeamsPlayedMatch(List<Match> matches, int team1Id, int team2Id) {
        return matches.stream().anyMatch(m -> m.getTeam1Id() == team1Id && m.getTeam2Id() == team2Id
                || m.getTeam1Id() == team2Id && m.getTeam2Id() == team1Id);
    }
    public static void generateTeamsAndPlayers (){
        DaoManager.MATCH_DAO.deleteAll();
        DaoManager.TEAM_DAO.deleteAll();
        DaoManager.PLAYER_DAO.deleteAll();
        Random random = new Random();
        for (int i =0; i<15; i++){
            String name = "Drużyna"+(i+1);
            int year1 = 2010 - random.nextInt(100);
            LocalDate date = LocalDate.of(year1,(1+random.nextInt(12)),(1+random.nextInt(30)));
            Team team = new Team(name, date,"brak" );
            DaoManager.TEAM_DAO.add(team);
            for (int j=0; j<20;j++){
                String playerName = "Zawodnik "+(j+1);
                String playerSurname = "Auto";
                int year = 2010 - random.nextInt(20);
                LocalDate dateOfBirth = LocalDate.of(year,(1+random.nextInt(12)),(1+random.nextInt(27)));
                Player player = new Player(team.getId(),playerName, playerSurname, dateOfBirth,random.nextInt(100),random.nextInt(100),random.nextInt(100) );
                DaoManager.PLAYER_DAO.add(player);
            }
        }

    }
}


